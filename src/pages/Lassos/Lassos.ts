import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { NewLessoPage } from '../new-lesso/new-lesso';

@Component({
  selector: 'page-Lassos',
  templateUrl: 'Lassos.html'
})
export class Lassos {
  items=[];
  constructor(public navCtrl: NavController) {
    this.items = [
      {
        'title': 'Back Pack',
        'image':'../assets/icon/backPack.png',
        'isChecked':true
      },
      {
        'title': 'Keys',
        'image':'../assets/icon/key.png',
        'isChecked':false
      },
      {'title': 'Laptops',
      'image':'../assets/icon/laptop.png',
      'isChecked':true
     }
    ]

  }
  doClick(){
    this.navCtrl.push(NewLessoPage);
  }

}
