import { Component } from '@angular/core';

import {  Lassos } from '../Lassos/Lassos';
import { AlaramPage } from '../Alaram/Alaram';
import { MasterDevices } from '../MasterDevices/MasterDevices';
import { SyncPage } from '../sync/sync';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = MasterDevices;
  tab2Root = Lassos;
  tab3Root = AlaramPage;
  tab4Root = SyncPage;

  constructor() {

  }
}
