import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DavidsAlaramPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-davids-alaram',
  templateUrl: 'davids-alaram.html',
})
export class DavidsAlaramPage {

  items=[];
  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.items = [
      {
        'title': 'Back Pack',
        'image':'../assets/icon/backPack.png',
        'isChecked':true
      },
      {
        'title': 'Keys',
        'image':'../assets/icon/key.png',
        'isChecked':false
      },
      {'title': 'Laptops',
      'image':'../assets/icon/laptop.png',
      'isChecked':true
     }
    ]

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DavidsAlaramPage');
  }

}
