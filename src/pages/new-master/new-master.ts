import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-new-master',
  templateUrl: 'new-master.html',
})
export class NewMasterPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
