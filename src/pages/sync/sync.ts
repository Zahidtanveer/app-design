import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Sync2Page } from '../sync2/sync2';
/**
 * Generated class for the SyncPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-sync',
  templateUrl: 'sync.html',
})
export class SyncPage {
  items=[];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.items = [
      {
        'title': 'Kerry Park',
        'image':'../assets/imgs/profile3.jpg',
        'description': 'A powerful Javascript framework for building single page apps. Angular is open source, and maintained by Google.',
      },
      {
        'title': 'David Smith',
        'image':'../assets/imgs/david.jpg',
        'description': 'A powerful Javascript framework for building single page apps. Angular is open source, and maintained by Google.',
      }
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SyncPage');
  }
  doClick(){
    this.navCtrl.push(Sync2Page);

  }

}
