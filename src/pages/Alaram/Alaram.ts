import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DavidsAlaramPage } from '../davids-alaram/davids-alaram';

@Component({
  selector: 'page-Alaram',
  templateUrl: 'Alaram.html'
})
export class AlaramPage {
  items = [];
  constructor(public navCtrl: NavController) {
    this.items = [
      {
        'title': 'Kerry Park',
        'image':'../assets/imgs/profile3.jpg',
        'description': 'A powerful Javascript framework for building single page apps. Angular is open source, and maintained by Google.',
      },
      {
        'title': 'David Smith',
        'image':'../assets/imgs/david.jpg',
        'description': 'A powerful Javascript framework for building single page apps. Angular is open source, and maintained by Google.',
      }
    ]
  }
  doClick(){
    this.navCtrl.push(DavidsAlaramPage);

  }

}
