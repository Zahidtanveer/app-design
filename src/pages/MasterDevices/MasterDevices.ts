import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewMasterPage } from '../new-master/new-master';

@Component({
  selector: 'page-MasterDevices',
  templateUrl: 'MasterDevices.html'
})
export class MasterDevices {
  items = [];
  constructor(public nav: NavController) {
    this.items = [
      {
        'title': 'Kerry Park',
        'image':'../assets/imgs/profile3.jpg',
        'description': 'A powerful Javascript framework for building single page apps. Angular is open source, and maintained by Google.',
      },
      {
        'title': 'David Smith',
        'image':'../assets/imgs/david.jpg',
        'description': 'A powerful Javascript framework for building single page apps. Angular is open source, and maintained by Google.',
      }
    ]
  }
  openNavDetailsPage(item) {
    this.nav.push(NewMasterPage, { item: item });
  }
}
