import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { Lassos } from '../pages/Lassos/Lassos';
import { AlaramPage } from '../pages/Alaram/Alaram';
import { SyncPage } from '../pages/sync/sync';
import { NewMasterPage } from '../pages/new-master/new-master';
import { MasterDevices } from '../pages/MasterDevices/MasterDevices';
import { NewLessoPage } from '../pages/new-lesso/new-lesso';
import { TabsPage } from '../pages/tabs/tabs';
import { Sync2Page } from '../pages/sync2/sync2';
import { DavidsAlaramPage } from '../pages/davids-alaram/davids-alaram';


import { MyApp } from './app.component';

@NgModule({
  declarations: [
    MyApp,
    Lassos,
    AlaramPage,
    MasterDevices,
    SyncPage,
    NewMasterPage,
    NewLessoPage,
    DavidsAlaramPage,
    Sync2Page,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Lassos,
    AlaramPage,
    MasterDevices,
    SyncPage,
    NewMasterPage,
    NewLessoPage,
    DavidsAlaramPage,
    Sync2Page,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
